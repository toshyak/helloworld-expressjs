# helloworld-expressjs

A simple hello world application written in NodeJS using the expressJS framework

Install

`npm install`

Run

`npm run`

Environment

- LISTEN_PORT: set the listen port, default 3000
- APP_NAME: set the app name, default "not set"

## CI/CD

New version of docker image is built on every commit and stored in Gitlab container registry. Image tag is generated automatically from git commit SHA. For commits and merges to master branch image is also tagged with `latest`. For tagged commits image is also tagged with this tag and deployed to production kubernetes cluster.

Helm is used for deploying to kubernetes cluster. Deployment manifests can be configured with [values](charts/helloworld-expressjs/values.yaml) file.


## TODO
- [ ] Expose deployment to the Internet. Can be done either with `service.type: LoadBalancer` or `Ingress` resource, depending on Kubernetes cluster configuration
- [ ] Add kubernetes cluster config to deploy_production step in .gitlab-ci.yml
- [ ] Add review step to gitlab-ci.yml. Can be done with helm parameters for chart
- [ ] Auto-rollback for failed deployments. Can be done with gitlab CI and helm, but would be easier to have specific tooling for it