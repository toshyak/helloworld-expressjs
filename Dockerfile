FROM node:15.12-alpine3.13

WORKDIR /usr/src/app

COPY app/package*.json ./
RUN npm install

# Bundle app source
COPY app/ .

RUN addgroup -S appgroup && adduser -S appuser -G appgroup && \
    chown -R appuser:appgroup /usr/src/app
USER appuser

CMD [ "node", "index.js" ]